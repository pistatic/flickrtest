import { Injectable } from '@angular/core';
import { ResultList, ResultItem, ImageDetails } from './results/result_item';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor() { }
  
  public returnCurrentPage( data:ResultList, pageNum:number, pageItemsMax:number ):ResultList{
	  var slicedData:ResultItem[] = data.results.slice(pageNum*pageItemsMax,((pageNum+1)*pageItemsMax))
	  return {results:slicedData};
  }
  
  private range(lowest:number, highest:number){
	var arr = [];
	for( var i=lowest; i<=highest; i++){ arr.push(i); }
	return arr;
  }
  
  public calculatePageNavArray( numPages:number, currentPage:number, navLength:number ){
	if(numPages <= navLength){ //eg (0) 1 2
		return this.range(0,numPages-1);
	}
	if(currentPage < Math.ceil(navLength/2)){ //eg 0 (1) 2 3 4
		return this.range(0,navLength-1);
	}
	if(numPages-currentPage < Math.ceil(navLength/2)){ //eg 12 13 14 (15) 16
		return this.range(numPages-navLength,numPages-1);
	}
	return this.range(currentPage-2,currentPage+2); //eg 6 7 (8) 9 10
  }
}
