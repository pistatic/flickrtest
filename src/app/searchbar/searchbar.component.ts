import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, ViewChild, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { FetchcategoriesService } from '../fetchcategories.service';
import { Categories, Maincat, Subcat } from '../fetchcategories.service';
@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {

  private maincats:Maincat[];

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredCategories: Observable<Maincat[]>;
  tags: string[] = [];

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;

  constructor( private catFetch:FetchcategoriesService ) {

	
	this.catFetch.fetchCategories().subscribe(fetchedData => {
		this.maincats = fetchedData;
		
		this.filteredCategories = this.tagCtrl.valueChanges.pipe(
			startWith(''),
			map((tag:string) => this._filterGroup(tag))
		);	
	});
    

  }
  ngOnInit() {
  }
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
	
    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }
    if (input) {
      input.value = '';
    }

    this.tagCtrl.setValue(null);
  }

  remove(tag: string): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.value.parentName);
	this.tags.push(event.option.value.name);
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
  }

  private _filterGroup(value: string):Maincat[] {
	if (value) {
      return this.maincats
        .map(group => ({name: group.name, subcats: this._filter(group.subcats, value)}))
        .filter(group => group.subcats.length > 0);
    }
    return this.maincats;
  }
  
  private _filter = (opt: Subcat[], value: string): Subcat[] => {
    const filterValue = value.toLowerCase ? value.toLowerCase() : "";
    return opt.filter(item => item.name.toLowerCase().indexOf(filterValue) >= 0);
  };
  
  public getTags():string{
	return this.tags.join(",");
  }
  
}