import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule}   from '@angular/forms';

import { AppComponent } from './app.component';
import { ResultsComponent } from './results/results.component';
import { HttpClientModule } from '@angular/common/http';
import { FetchdataService } from './fetchdata.service';
import { SearchbarComponent } from './searchbar/searchbar.component';

import { AppmaterialsModule } from './appmaterials.module';

@NgModule({
  declarations: [
    AppComponent,
    ResultsComponent,
    SearchbarComponent,
  ],
  imports: [
    BrowserModule,

	BrowserAnimationsModule,
	FormsModule,
	ReactiveFormsModule,
	HttpClientModule,
	AppmaterialsModule
  ],
  providers: [FetchdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
