import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Rx';
import { ResultList, ResultItem, ImageDetails } from './results/result_item';

@Injectable({
  providedIn: 'root',
})
export class FetchdataService {
  private data = {} as ResultList;
  constructor(private http: HttpClient) { }
  fetchData(tags): Observable<any>{
   return this.http.get<any>('https://api.flickr.com/services/rest/?method=flickr.photos.search'+
		'&api_key=57f694132e4714c29a64c9af890b124e'+
		'&tags='+tags+
		'&tag_mode=all'+
		'&&format=json'+
		'&nojsoncallback=1',
		{responseType: 'json'}).map(res => {
			this.data.results = [];
			for(var photo of res.photos.photo){
				var currPhoto:ResultItem = {title:photo.title, id:photo.id}
				this.data.results.push(currPhoto);
			}
			return this.data;
    });
  }
  
  fetchImage(imgid):Observable<any>{
	return this.http.get<any>('https://api.flickr.com/services/rest/?method=flickr.photos.getSizes'+
		'&api_key=57f694132e4714c29a64c9af890b124e'+
		'&photo_id='+imgid+
		'&&format=json'+
		'&nojsoncallback=1',
		{responseType: 'json'}).map(res => {
			return res;
    });
  }
  
  fetchImageInfo(imgid):Observable<any>{
	return this.http.get<any>('https://api.flickr.com/services/rest/?method=flickr.photos.getInfo'+
		'&api_key=57f694132e4714c29a64c9af890b124e'+
		'&photo_id='+imgid+
		'&&format=json'+
		'&nojsoncallback=1',
		{responseType: 'json'})
  }
  
  returnData(){
	return this.data;
  }
  
  
}
