import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'
import {Observable} from 'rxjs/Rx';

export interface Categories {
	maincats: Maincat[];
    }
export interface Maincat {
	name: string;
	subcats: Subcat[];
    }
export interface Subcat {
	name: string;
	parentName?: string;
}

@Injectable({
  providedIn: 'root'
})


export class FetchcategoriesService {

  constructor(private http: HttpClient) { }
  fetchCategories(): Observable<Maincat[]>{
	return this.http.get<Categories>('resources/categories.txt').map(res => {
			var maincategories = res.maincats;
			for(var maincat of maincategories){
				for(var subcat of maincat.subcats){
					subcat.parentName = maincat.name;
				}	
			}
			return maincategories;
    });	
  }
}
