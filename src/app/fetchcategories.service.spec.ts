import { TestBed } from '@angular/core/testing';

import { FetchcategoriesService } from './fetchcategories.service';

describe('FetchcategoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchcategoriesService = TestBed.get(FetchcategoriesService);
    expect(service).toBeTruthy();
  });
});
