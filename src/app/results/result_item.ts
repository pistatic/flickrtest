export interface ResultList {
	results: ResultItem[];
    }
export interface ResultItem {
	title: string;
	id: string;
	details?: ImageDetails;
    }
export interface ImageDetails {
	imgsrc?: string;
	desc?: string;
	date?: string;
	tags?: string[];
}