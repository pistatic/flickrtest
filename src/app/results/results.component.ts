import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResultList, ResultItem, ImageDetails } from './result_item';
import { FetchdataService } from '../fetchdata.service';
import { PagesService } from '../pages.service';
import { SearchbarComponent } from "../searchbar/searchbar.component";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css'],
})
export class ResultsComponent implements OnInit {

  @ViewChild(SearchbarComponent) searchbar;
  constructor( private fetcher:FetchdataService, private ps:PagesService ) { }
  private resultData:ResultList;
  private currentPageNum:number = 0;
  readonly pageBarLength = 5;
  private pagesTotalArr:number[] = [0,1,2];
  private pageItemsMax:number = 9;
  private numberOfPages:number = 1;
  private pageData:ResultList;
  private mediumSizeIndex:number = 6;
  ngOnInit() {
	var loadingImg=new Image();
    loadingImg.src="/resources/loading_icon.gif";
  }
  
  onSubmit() {
	this.fetcher.fetchData(this.searchbar.getTags()).subscribe(fetchedData => {
		this.resultData = fetchedData;
		this.numberOfPages = Math.ceil(this.resultData.results.length/this.pageItemsMax);
		this.changePage(0);
	})
	//console.log(this.searchbar.getTags())
	
    }
	
  public changePage(pageNum){
	if(pageNum<0 || pageNum>=this.numberOfPages){
		return;
	}
	this.currentPageNum = pageNum;
	this.pagesTotalArr = this.ps.calculatePageNavArray(this.numberOfPages ,pageNum, this.pageBarLength)
	this.pageData = this.ps.returnCurrentPage(this.resultData,pageNum,this.pageItemsMax);
	for(let p of this.pageData.results){
		if(!p.details){
			p.details = {};
			p.details.imgsrc = "/resources/loading_icon.gif"
			this.fetcher.fetchImage(p.id).subscribe(fetchedSizes => {
				p.details.imgsrc = fetchedSizes.sizes.size[this.mediumSizeIndex].source;
			})
			this.fetcher.fetchImageInfo(p.id).subscribe(fetchedInfo => {
				p.details.desc = fetchedInfo.photo.description._content;
				p.details.date = fetchedInfo.photo.dates.taken;
				p.details.tags = fetchedInfo.photo.tags.tag.map(x => x._content);
			})
		}
	}
  }
	

}
